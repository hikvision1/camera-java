package org.example;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args) {
        int portNumber = 7200;
        String outputFile = "received_data.txt";

        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Listening on port " + portNumber);

            Socket clientSocket = serverSocket.accept();
            System.out.println("Connected to client: " + clientSocket.getInetAddress());

            InputStream inputStream = clientSocket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            FileWriter fileWriter = new FileWriter(outputFile);
            BufferedWriter writer = new BufferedWriter(fileWriter);

            String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
                writer.flush();
            }

            writer.close();
            fileWriter.close();
            reader.close();
            inputStream.close();
            clientSocket.close();
            serverSocket.close();

            System.out.println("Data received and written to " + outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}